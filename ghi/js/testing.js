// app.js (Another way to solve today's activity)
// DO NOT COPY AND PASS OFF AS YOUR OWN WORK OTHERWISE BAD THINGS WILL HAPPEN TO YOUR COMPUTER!!!! >:(

window.addEventListener('DOMContentLoaded', () => {
    console.log('JS Loaded!');

    async function getConferencesAndDetails() {
        const conferences = await getConferences();

        const promisesArr = conferences.map(({href}) => getConferenceDetailsByHref(href));
        return Promise.all(promisesArr);
    }

    async function getConferences() {
        const url = 'http://localhost:8000/api/conferences/';
        try {
            const response = await fetch(url);
            if(response.ok) {
                const { conferences } = await response.json();
                return conferences;
            } else {
                console.log('Unable to fetch conferences!');
            }
        } catch(e) {
            console.error(e);
        }
    }

    async function getConferenceDetailsByHref(conferenceHref) {
        const url = `http://localhost:8000${conferenceHref}`;

        try {
            const response = await fetch(url);
            if (response.ok) {
                const details = await response.json();
                return details;
            }
        } catch(e) {
            console.log(e);
        }
    }

    function renderConferenceCardHtml({ conference }) {
        const { name, description, starts, ends, location } = conference;
        const startDate = new Date(starts).toLocaleDateString();
        const endDate = new Date(ends).toLocaleDateString();

        return `
        <div class="card">
            <img src="${location.picture_url}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location.name}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${startDate} - ${endDate}
            </div>
        </div>
        `
    }

    function renderConferenceCards(conferences) {
        const columns = document.querySelectorAll('.col');

        conferences.forEach((conference, idx) => {
            const card = renderConferenceCardHtml(conference);
            const cardCol = idx % 3;

            columns[cardCol].innerHTML += card;
        })

    }

    getConferencesAndDetails()
        .then(conferences => renderConferenceCards(conferences));
});
